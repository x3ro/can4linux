Kvaser PCIcan-Q 

For all older CAN boards exists a PDF document UG_98048_pcican_userguide.pdf

PCIcan-Q has four CAN controllers.

PCIcan board uses the S5920 from AMCC as PCI controller.
The PCI controller is responsible for address decoding and interrupt steering.

The PCI controller
can decode up to 5 different address areas,
three of which are used by the PCIcan.

Address area   Type Size (bytes) Used for
    0          I/O  128          AMCC registers.
                                 Described in the S5920 manual.
    1          I/O  128          SJA1000 circuits
                                 0x00 - 0x1f: SJA1000 #1
                                 0x20 - 0x3F: SJA1000 #2
                                 0x40 - 0x5F: SJA1000 #3
                                 0x60 - 0x7F: SJA1000 #4
    2          I/O  8            Xilinx registers

Test


Address area number 1,
the one used for the SJA1000's,
is further subdivided into four areas
of 32 bytes each;
one for each (possible) SJA1000.

The S5920 is operated in pass-thru operation, passive mode.
To configure the address areas,
the value 0x80808080
should be written
into the PCI PASSTHRU CONFIGURATION REGISTER (PTCR) register.
This sets all regions to use 0 wait
states and to use the PTADR signal.

The PCIcan uses one PCI bus interrupt, INTA#.
It is asserted whenever one
or more
SJA1000's
have their interrupts active.
To reset an active interrupt,
read the interrupt status register in all present SJA1000s
- the interrupt of the corresponding SJA1000 will then
automatically clear.
To check the status of the interrupt line,
test the INTERRUPT ASSERTED bit (number 23)
in the INTCSR register in the S5920.
To enable or disable interrupts from the PCIcan,
use the ADD-ON INTERRUPT PIN ENABLE (bit 13)
in the INTCSR register in the S5920.


Xilinx FPGA

The Xilinx FPGA implements a few registers.
Address offset   Register   Usage
  0-6                       Reserved, do not use
  7              VERINT     Bit 7 - 4 contains the revision number of the FPGA
                            configuration. 15 is the first revision,
                            14 is the next, and so on.

The current FPGA revision number is 14
(which is read from the VERINT register as 1110xxxx).
Future revisions (13, 12, 11, ...) will remain compatible with revision 14.



PCI Configuration Data
The following data are configured automatically
into the S5920 PCI controller
when power is applied to the card.
 Item                           Value
 Vendor Id                      0x10e8
 Device Id                      0x8406 (for all PCIcan boards)
 Revision Id                    0
 Class Code                     0xffff00 (means:
 					no base class code defined for device)
 Subsystem Vendor Id            0
 Subsystem Device Id            0

Configuration of the SJA1000

Setting the OCR register to 0xDA is a good idea. 


References
AMCC PCI Products Data Book S5920 / S5933 (1998)
     Also available on the web (www.amcc.com) in the file pciprod.pdf.


$ lspci
0000:00:0c.0 Class ff00: Applied Micro Circuits Corp.: Unknown device 8406


0000:00:0c.0 Class ff00: Applied Micro Circuits Corp.: Unknown device 8406
        Flags: medium devsel, IRQ 10
        I/O ports at de00 [size=128]
        I/O ports at dc00 [size=128]
        I/O ports at da00 [size=8]
==============================================================================

As of kernel 5.x	(Sept. 2019)
Used to have a new Kvaser Board!  It is one with an Altera FPGA implementing
4 CAN FD Controllers with a specifik Kvaser IP
As of April 2020 this one is not supported (yet?) by can4linux
 
$ lspci
02:00.0 CANBUS: Kvaser AB Device 000d (rev 01)

$ sudo lspci -s 02:00.0 -vvv
02:00.0 CANBUS: Kvaser AB Device 000d (rev 01)
        Subsystem: Kvaser AB Device 000d
        Control: I/O+ Mem+ BusMaster+ SpecCycle- MemWINV- VGASnoop- ParErr- Stepping- SERR+ FastB2B- DisINTx-
        Status: Cap+ 66MHz- UDF- FastB2B- ParErr- DEVSEL=fast >TAbort- <TAbort- <MAbort- >SERR- <PERR- INTx-
        Latency: 0, Cache Line Size: 64 bytes
        Interrupt: pin A routed to IRQ 10
        NUMA node: 0
        Region 0: Memory at fe9e0000 (32-bit, non-prefetchable) [size=128K]
        Capabilities: [50] MSI: Enable- Count=1/1 Maskable- 64bit+
                Address: 0000000000000000  Data: 0000
        Capabilities: [78] Power Management version 3
                Flags: PMEClk- DSI- D1- D2- AuxCurrent=0mA PME(D0-,D1-,D2-,D3hot-,D3cold-)
                Status: D0 NoSoftRst- PME-Enable- DSel=0 DScale=0 PME-
        Capabilities: [80] Express (v1) Endpoint, MSI 00
                DevCap: MaxPayload 128 bytes, PhantFunc 0, Latency L0s <64ns, L1 <1us
                        ExtTag- AttnBtn- AttnInd- PwrInd- RBE+ FLReset- SlotPowerLimit 75.000W
                DevCtl: CorrErr- NonFatalErr- FatalErr- UnsupReq-
                        RlxdOrd- ExtTag- PhantFunc- AuxPwr- NoSnoop+
                        MaxPayload 128 bytes, MaxReadReq 512 bytes
                DevSta: CorrErr- NonFatalErr- FatalErr- UnsupReq- AuxPwr- TransPend-
                LnkCap: Port #1, Speed 2.5GT/s, Width x1, ASPM L0s, Exit Latency L0s unlimited
                        ClockPM- Surprise- LLActRep- BwNot- ASPMOptComp-
                LnkCtl: ASPM Disabled; RCB 64 bytes Disabled- CommClk+
                        ExtSynch- ClockPM- AutWidDis- BWInt- AutBWInt-
                LnkSta: Speed 2.5GT/s (ok), Width x1 (ok)
                        TrErr- Train- SlotClk+ DLActive- BWMgmt- ABWMgmt-
        Capabilities: [100 v1] Virtual Channel
                Caps:   LPEVC=0 RefClk=100ns PATEntryBits=1
                Arb:    Fixed+ WRR32- WRR64- WRR128-
                Ctrl:   ArbSelect=Fixed
                Status: InProgress-
                VC0:    Caps:   PATOffset=00 MaxTimeSlots=1 RejSnoopTrans-
                        Arb:    Fixed- WRR32- WRR64- WRR128- TWRR128- WRR256-
                        Ctrl:   Enable+ ID=0 ArbSelect=Fixed TC/VC=ff
                        Status: NegoPending- InProgress-

-k 
-xxx 

sudo /sbin/lspci -s 02:00.0 -xxx
02:00.0 CANBUS: Kvaser AB Device 000d (rev 01)
00: 07 1a 0d 00 07 01 10 00 01 00 09 0c 10 00 00 00
10: 00 00 9e fe 00 00 00 00 00 00 00 00 00 00 00 00
20: 00 00 00 00 00 00 00 00 00 00 00 00 07 1a 0d 00
30: 00 00 00 00 50 00 00 00 00 00 00 00 0a 01 00 00
40: 00 00 00 00 60 61 00 02 00 00 00 00 00 00 00 00
50: 05 78 80 00 00 00 00 00 00 00 00 00 00 00 00 00
60: 00 00 00 00 00 00 00 00 11 78 00 00 00 00 00 00
70: 00 00 00 00 00 00 00 00 01 80 03 00 00 00 00 00
80: 10 00 01 00 00 80 2c 01 00 28 00 00 11 f4 03 01
90: 40 00 11 10 00 00 04 00 c0 03 00 00 00 00 00 00
a0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
b0: 01 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00
c0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
d0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
e0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
f0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00

===============================================================

------------

sudo cat /proc/iomem| less
 fe900000-fe9fffff : PCI Bus 0000:02
    fe9e0000-fe9fffff : 0000:02:00.0
--------------
EMS PCI -s 05:05.0
Subsystem: Device ff00:0000  

Region 0: Memory at febff000 
Region 1: Memory at febfe000 

sudo cat /proc/iomem
 feb00000-febfffff : PCI Bus 0000:05
    febfe000-febfefff : 0000:05:05.0
    febff000-febfffff : 0000:05:05.0

setpci -s <BUS_ADDR> COMMAND=0x02

