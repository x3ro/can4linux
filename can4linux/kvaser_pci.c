
/* Kvaser PCICan-4HS specific stuff
 * 
 * (c) 2006-2012 oe@port.de
 * (c) 2006-2020  hj.oertel@t-online.de
 *
 * Parts of this software are based on (derived) the following:
 * - Kvaser linux driver, version 4.72 BETA
 *
  PCI Addresses
  04:02.0 ff00: 10e8:8406
        Control: I/O+ Mem+ BusMaster- SpecCycle- MemWINV- VGASnoop- ParErr- 
	Stepping- SERR- FastB2B- DisINTx-
        Status: Cap- 66MHz- UDF- FastB2B+ ParErr- DEVSEL=medium >TAbort- 
	<TAbort- <MAbort- >SERR- <PERR- INTx-
        Interrupt: pin A routed to IRQ 18
        Region 0: I/O ports at 2080 [size=128]
        Region 1: I/O ports at 2000 [size=128]
        Region 2: I/O ports at 2100 [size=8]
 
  02:00.0 CANBUS [0c09]: Kvaser AB Device [1a07:000d] (rev 01)
        Memory at fe9e0000 (32-bit, non-prefetchable) [size=128K]
 *
 * Compliant with PCI 2.2.
 * I/O mapped.
 * CAN Controllers: SJA1000 from Philips, with 64-byte receive FIFO.
 * Supports CAN 2.0 A and 2.0 B (active).
 * High-speed ISO 11898 compliant driver circuits.
 * Interfaces the CAN bus using a DSUB CAN connector.
 * CAN oscillator frequency is 16 MHz.
 * Galvanic isolation between the CAN-controller and the CAN-driver.
 * The common CAN bus has a built-in optional termination.
 *
 * (end of life reached)
 *
 */

/* use it for pr_info() and consorts */
#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/pci.h>
#include "defs.h"

# ifndef CONFIG_PCI
#   error "trying to compile a PCI driver for a kernel without CONFIG_PCI"
# endif
#define DRV_NAME "Kvaser PCI-Q"

/* used for storing the global pci register address */
/* one element more than needed for marking the end */
struct	pci_dev *can_pcidev[MAX_CHANNELS + 1] = { NULL };


/* PCI Bridge AMCC 5920 registers */
#define S5920_OMB    0x0C /* Outgoing Mailbox Register */
#define S5920_IMB    0x1C /* Incoming Mailbox Register */
#define S5920_MBEF   0x34 /* Mailbox Empty/Full Status Register */
#define S5920_INTCSR 0x38 /* Interrupt Control/Status Register */
#define S5920_RCR    0x3C /* Reset Control Register  */
#define S5920_PTCR   0x60 /* pass thru configuration register */

#define INTCSR_ADDON_INTENABLE_M        0x2000
#define INTCSR_INTERRUPT_ASSERTED_M     0x800000


int pcimod_scan(void)
{
struct	pci_dev *pdev = NULL;
int	candev = 0;			/* number of devices found so far */
int	nextcandev;
int 	err;

    for_each_pci_dev(pdev) {
	/*
	pr_info("Scan with vendor: 0x%0x, device: 0x%0x\n", 
		    pdev->vendor, pdev->device);
	*/
	if(((pdev->vendor == PCI_VENDOR_CAN_KVASER_ID1) | (pdev->vendor == PCI_VENDOR_CAN_KVASER_ID2))
	&& ((pdev->device == PCI_DEVICE_CAN_KVASER_ID1) | (pdev->device == PCI_DEVICE_CAN_KVASER_ID2))) {
	    if((pdev->vendor == PCI_VENDOR_CAN_KVASER_ID2)
		    && (pdev->device == PCI_DEVICE_CAN_KVASER_ID2))  {
		pr_err("   found KVASER-PCICAN V2 CANFD board: %s\n", pci_name(pdev));
		pr_info("   this driver needs the old board 4 x sja1000\n");
		err = -ENODEV;
		continue;
		// goto failure;
	    }
	    if((pdev->vendor == PCI_VENDOR_CAN_KVASER_ID1)
		    && (pdev->device == PCI_DEVICE_CAN_KVASER_ID1))  {
		pr_info("found KVASER-PCICAN V1: %s\n", pci_name(pdev));
	    }

	    err = pci_enable_device(pdev);
	    if (err) {
		    continue;
	    }

	    // pr_info("PCI bus speed = 0x%x\n", pcie_get_speed_cap(pdev)); 
	    pr_info("initializing device %04x:%04x\n",
		     pdev->vendor, pdev->device);
	    pr_info("      using IRQ %d\n", pdev->irq);
	    /* PCI Controller is an S5920 from AMCC
	     *
	     * this is the pci register range S5920
	     * To configure the address areas,
	     * the value 0x80808080 should be written into the
	     * PCI PASSTHRU CONFIGURATION REGISTER (PTCR) register.
	     * This sets all regions to use 0 wait states and to use the PTADR signal.
	     * */
	    if ((pci_resource_flags(pdev, 0)) & IORESOURCE_IO) {
		    pr_info("  resource 0 IO size: %ld\n", 
			    (long)pci_resource_len(pdev, 0) );
		if(pci_request_region(pdev, 0, "kv_can_s5920") != 0)
		    return -ENODEV;

	    } else if((pci_resource_flags(pdev, 0)) & IORESOURCE_MEM) {
		    pr_info("  resource 0 MEM");
	    }
	    pr_info("  got PCI register region\n");

	    /* this is the CAN  I/O register range */
	    if ((pci_resource_flags(pdev, 1)) & IORESOURCE_IO) {
		    pr_info("  resource 1 IO size:  %ld\n", 
			    (long)pci_resource_len(pdev, 1) );
		if(pci_request_region(pdev, 1, "kv_can_sja1000") != 0)
		    goto error_io;

	    } else if((pci_resource_flags(pdev, 1)) & IORESOURCE_MEM) {
		    pr_info("  resource 1 MEM");
	    }
	    pr_info("  got CAN controller register region\n");

	    /* this is the Xilinx register range */
	    if ((pci_resource_flags(pdev, 2)) & IORESOURCE_IO) {
		    pr_info("  resource 2 IO size:  %ld\n", 
			    (long)pci_resource_len(pdev, 2) );
		if(pci_request_region(pdev, 2, "kv_can_xilinx") != 0)
		    goto error_xilinx;

	    } else if((pci_resource_flags(pdev, 2)) & IORESOURCE_MEM) {
		    pr_info("  resource 2 MEM");
	    }

	    pr_info("  got XILINX region\n");
	    /* Read version info from xilinx chip
	     * The current FPGA revision number is 14
	     * (which is read from the VERINT register as 1110xxxx).
	     * Future revisions (13, 12, 11, ...)
	     * will remain compatible with revision 14. */
	    pr_info("  Xilinx chip version %d\n",
		    (inb(pci_resource_start(pdev, 2) + 7) >> 4));

	    /* Assert PTADR#
	     * - we're in passive mode so the other bits are not important */
	    outl(0x80808080UL, pci_resource_start(pdev, 0) + S5920_PTCR);

	    /* Loop through the io area 1 to see how many CAN controllers */
	    /* are on board (1, 2 or 4)					  */
	    /* be prepared that this happens for each board		  */

	    nextcandev = candev + 4;   /* the PCICan has max four Controllers */
	    for(; candev < nextcandev; candev++) {
	    unsigned long io;
		can_pcidev[candev] = pdev;
		io = pci_resource_start(pdev, 1) + (candev * 0x20);
		// base_addr += (candev * 0x20);
		// pr_info("  check for CAN at pos %d, io address %p\n", candev + 1, base_addr);
		if(controller_available(io, 1)) {
		    pr_info("  CAN at pos %d, io address 0x%04X\n", candev + 1, (unsigned)io);
		    if(candev > MAX_CHANNELS) {
			pr_info("  CAN: only %d devices supported\n", MAX_CHANNELS);
			break; /* the devices scan loop */
		    }
		    proc_base[candev] = io;
		    IRQ[candev] = pdev->irq;
		    proc_iomodel[candev] = 'p';
		    /* all CAN channels use the same clock */
		    proc_clock = 8000000;

		    /* can_dump(candev); */
		}
		/* its the same dev, the pointer is board global,
		and should be the same for all 4 devices */
		// pr_info("  ==> candev %d : pointer %p", candev, can_pcidev[candev]);
	    }

	    /* may be there is something pending */
	    disable_pci_interrupt(pci_resource_start(pdev, 0));


	} /* if KVASER */
    } /* for_each_pci_dev() */
    if (candev == 0)
    	err = -ENODEV;
    else
	return 0;

error_xilinx:
	pci_release_region(pdev, 1);   /*release i/o */

error_io:
    pci_release_region(pdev, 0);   /*release pci */
	return -ENODEV;
}

/* Called from __init,  once when driver is loaded
   set up physical addresses, irq number
   and initialize clock source for the CAN module

   take care it will be called only once
   because it is called for every CAN channel out of MAX_CHANNELS
*/
int init_board_hw(int n)
{
static int already_called = 0;
int ret;
int minor = -1;

	// DBGIN();
	ret = 0;
	if (!already_called && virtual == 0) {
	DBGIN();
		/* make some sysctl entries read only
		 * IRQ number
		 * Base address
		 * and access mode
		 * are fixed and provided by the PCI BIOS
		 */
		can_sysctl_table[CAN_SYSCTL_IRQ - 1].mode = 0444;
		can_sysctl_table[CAN_SYSCTL_BASE - 1].mode = 0444;
		/* printk(KERN_INFO "CAN pci test loaded\n"); */
		/* proc_bgmask = 0; */
		if (pcimod_scan()) {
			pr_err("  no valid PCI CAN found");
			ret = -EIO;
		} else
		    pr_info("  pci scan success");
		already_called = 1;
	DBGOUT();
	}
	return ret;
}

void exit_board_hw(void)
{
int i;
void *ptr;

	i = 0;
	ptr = NULL;
	/* The pointer to dev can be used up to four times,
	 * but we have to release the region only once */
	while (can_pcidev[i]) {
		if (ptr != can_pcidev[i]) {

			pr_info("Can[-1]: - : Kvaser: release PCI resources\n");
			/* disable PCI board interrupts */
			disable_pci_interrupt(pci_resource_start
					      (can_pcidev[i], 0));
			/* pr_devel("release Kvaser CAN region 2 (XILINX)\n");*/
			pci_release_region(can_pcidev[i], 2);/*release xilinx */
			/* pr_devel("release Kvaser CAN region 1 (CAN)\n"); */
			pci_release_region(can_pcidev[i], 1);	/*release i/o */
			/* pr_devel("release Kvaser CAN region 0 (PCI)\n"); */
			pci_release_region(can_pcidev[i], 0);	/*release pci */

		}
		ptr = can_pcidev[i];
		i++;
	}
}

inline void disable_pci_interrupt(unsigned int base)
{
u32 tmp;
int minor = -1;

    DBGIN();	    
    // pr_info("disable pci int add 0x%x, 0x%x", base, base + S5920_INTCSR);

    /* Disable PCI interrupts from card */
    tmp = inl(base + S5920_INTCSR);
    tmp &= ~INTCSR_ADDON_INTENABLE_M;
    outl(tmp, base + S5920_INTCSR);
    DBGOUT();	    
}

inline void enable_pci_interrupt(unsigned int base)
{
u32 tmp;
int minor = -1;

    DBGIN();	    
    // pr_info("enable pci int add 0x%x, 0x%x\n", base, base + S5920_INTCSR);
    /* Enable PCI interrupts from card */
    tmp = inl(base + S5920_INTCSR);
    tmp |= INTCSR_ADDON_INTENABLE_M;
    outl(tmp, base + S5920_INTCSR);
    DBGOUT();	    
}






/* reset all CAN controllers on the Kvaser-PCI Board */
void reset_kvaser_pci(unsigned long address)
{
int minor = -1;
    DBGIN();
    DBGOUT()
}

/* check memory region if there is a CAN controller
*  assume the controller was resetted before testing 
*
*  The check for an avaliable controller is difficult !
*  After an Hardware Reset (or power on) the Conroller 
*  is in the so-called 'BasicCAN' mode.
*     we can check for: 
*         adress  name      value
*	    0x00  mode       0x21
*           0x02  status     0xc0
*           0x03  interrupt  0xe0
* Once loaded thr driver switches into 'PeliCAN' mode and things are getting
* difficult, because we now have only a 'soft reset' with not so  unique
* values. The have to be masked before comparing.
*         adress  name       mask   value
*	    0x00  mode               
*           0x01  command    0xff    0x00
*           0x02  status     0x37    0x34
*           0x03  interrupt  0xfb    0x00
*
*/
/* int controller_available(unsigned long address, int offset) */

int controller_available(upointer_t address, int offset)
{
int minor = -1;

    DBGIN();
    // pr_info("controller_available 0x%lx, offset %d\n",
    //				(unsigned long)address, offset);

#if 0
    pr_info("0x%0x, ", inb(address ) );
    pr_info("0x%0x, ", inb(address + (2 * offset)) );
    pr_info("0x%0x\n", inb(address + (3 * offset)) );
#endif

    /* Try to reset the CAN Controller before reading it.
     * Not really correct, in case it's not a CAN card, anyway.
     */
    outb(CAN_RESET_REQUEST, address);

    if ( 0x21 == inb(address))  {
	/* compare rest values of status and interrupt register */
	if(   0x0c == inb(address + 2)
	   && 0xe0 == inb(address + 3) ) {
	    return 1;
	} else {
	    return 0;
	}
    } else {
	/* may be called after a 'soft reset' in 'PeliCAN' mode */
	/*   value     address                     mask    */
	if(   0x00 ==  inb((address + 1))
	   && 0x34 == (inb((address + 2))    & 0x37)
	   && 0x00 == (inb((address + 3))    & 0xfb)
	  ) {
	    return 1;
        } else {
	    return 0;
        }
    }
}


int can_vendor_init(int minor)
{

    DBGIN();
    can_range[minor] = CAN_RANGE;
    
    /* Request the controllers address space
     * Nothing to do for the Kvaser PCICAN, we have io-addresses 
     * can_base in this case stores a (unsigned char *)
     *
     * CAN_PORT_IO only uses proc_base[]
     */

    /* test for valid IRQ number in /proc/sys/.../IRQ */
    if( IRQ[minor] > 0 && IRQ[minor] < MAX_IRQNUMBER ){
        int err;

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,18) 
	err = request_irq( IRQ[minor], can_interrupt, IRQF_SHARED, 
				"Can", &can_minors[minor]);
#else
	err = request_irq( IRQ[minor], can_interrupt, SA_SHIRQ, 
				"Can", &can_minors[minor]);
#endif

        if( !err ){
	    DBGPRINT(DBG_BRANCH,("Requested IRQ: %d @ 0x%lx",
				    IRQ[minor], (unsigned long)can_interrupt));
	    irq_requested[minor] = 1;
	} else {
	    DBGOUT(); return -EBUSY;
	}
    } else {
	/* Invalid IRQ number in /proc/.../IRQ */
	DBGOUT(); return -EBUSY;
    }

    enable_pci_interrupt(pci_resource_start(can_pcidev[minor], 0));

    DBGOUT(); return 0;
}


/* not needed ? */
void board_clear_interrupts(int minor)
{}

int can_freeirq(int minor, int irq )
{
    DBGIN();
    irq_requested[minor] = 0;
    /* pr_info(" Free IRQ %d  minor %d", irq, minor); */

    /* Disable Interrupt on the PCI board only if all channels
     * are not in use */
    if(    irq_requested[0] == 0
        && irq_requested[1] == 0 
        && irq_requested[2] == 0 
        && irq_requested[3] == 0 )
    /* and what happens if we only have 2 channels on the board,
       or we have minor == 4, thats a second board ??) */
    disable_pci_interrupt(pci_resource_start(can_pcidev[minor], 0));
    free_irq(irq, &can_minors[minor]);
    DBGOUT();
    return 0;
}
