
#include <linux/device.h>
#include <linux/can/dev.h>		/* SocketCAN */


#define KVASER_PCIEFD_DRV_NAME "can4linux_pciefd"


#define CAN_RANGE 0x4000	/* 4 channel x 0x1000 */

/* Bit timings with default SP@87.5% */
#ifdef SP87

#define CAN_TIME_10K	0x615A0027
#define CAN_TIME_20K	0x615A0013
#define CAN_TIME_50K	0x615A0007
#define CAN_TIME_100K	0x615A0003
#define CAN_TIME_125K	0x4D140003
#define CAN_TIME_250K	0x4D140001
#define CAN_TIME_500K	0x4D140000
#define CAN_TIME_800K	0x4C9C0000
#define CAN_TIME_1000K	0x4C740000
#define  CAN_FTIME_50K	0
#define  CAN_FTIME_100K	0
#define  CAN_FTIME_125K	0x4D140003
#define  CAN_FTIME_200K	0
#define  CAN_FTIME_250K	0x4D140001
#define  CAN_FTIME_400K	0
#define  CAN_FTIME_500K	0
#define  CAN_FTIME_800K	0
#define CAN_FTIME_1000K	0x24880000
	    /* @40tq, @20tq 85%  0x081e0001, at 10tq 80% 0x040c0003 */
#define CAN_FTIME_2000K	0x10420000 /* @40tq, 87.5% */
#define CAN_FTIME_4000K	0
#define CAN_FTIME_8000K	0
#endif

/* Timings according kvaser CAN bus tool using SP@80%
 * using 
 * http://www.bittiming.can-wiki.info/?CLK=80&ctype=KVASER-CANFD-ISO
 * */
#define CAN_TIME_10K	0x0
#define CAN_TIME_20K	0x0
#define CAN_TIME_50K	0x0
#define CAN_TIME_100K	0x0
#define CAN_TIME_125K	0x0c1c001f	/* 20tq, 80% sjw 1 */
#define CAN_TIME_250K	0x1c3c6007	/* 40 tq,  80% sjw 4 */
#define CAN_TIME_500K	 0x3c7de001	/* 80 tq, 80%, sjw 16 */
#define CAN_TIME_800K	0x0
#define CAN_TIME_1000K	0x1c3ce001	/* 40 tq, 80%, sjw 8 */
#define  CAN_FTIME_50K	0
#define  CAN_FTIME_100K	0
#define  CAN_FTIME_125K	0x0
#define  CAN_FTIME_200K	0
#define  CAN_FTIME_250K	0x3c7c0003	/* 80 tq, 80%, sjw 16 */
#define  CAN_FTIME_400K	0
#define  CAN_FTIME_500K	0x3c7c0001	/* 80 tq, 80%, sjw 16 */
#define  CAN_FTIME_800K	0
#define CAN_FTIME_1000K	0x1c3ce001	/* 40 tq, 80%, sjw 8 */
#define CAN_FTIME_2000K	0x0c1c6001	/* 20 tq, 80%, sjw 4 */
#define CAN_FTIME_4000K	0x040c2001	/* 10 tq, 80%, sjw 2 */
#define CAN_FTIME_8000K	0x040c0000	/* 10 tq, 80%, sjw 1 */

/* Defines copied from linux/drivers/net/can/kvaser_pciefd.c */

/* KCAN_IEN, KCAN_ISTAT, KCAN_IRQ - Interrupt register bits */

/* Tx FIFO unaligned read */
#define KVASER_PCIEFD_KCAN_IRQ_TAR BIT(0)
/* Tx FIFO unaligned end */
#define KVASER_PCIEFD_KCAN_IRQ_TAE BIT(1)
/* Bus parameter protection error */
#define KVASER_PCIEFD_KCAN_IRQ_BPP BIT(2)
/* FDF bit when controller is in classic mode */
#define KVASER_PCIEFD_KCAN_IRQ_FDIC BIT(3)
/* Idle State. Unit is in reset mode and no abort or flush is pending */
#define KVASER_PCIEFD_KCAN_IRQ_IDLE BIT(4)
/* Rx FIFO overflow */
#define KVASER_PCIEFD_KCAN_IRQ_ROF BIT(5)
/* Abort done */
#define KVASER_PCIEFD_KCAN_IRQ_ABD BIT(13)
/* Tx buffer flush done */
#define KVASER_PCIEFD_KCAN_IRQ_TFD BIT(14)	/* Transmit Buffer Flush Done */
/* Tx FIFO overflow */
#define KVASER_PCIEFD_KCAN_IRQ_TOF BIT(15)	/* Transmit FIFO overflow. */
/* Tx FIFO empty */
#define KVASER_PCIEFD_KCAN_IRQ_TE BIT(16)
/* Transmitter unaligned */
#define KVASER_PCIEFD_KCAN_IRQ_TAL BIT(17)

#define KVASER_PCIEFD_KCAN_TX_NPACKETS_MAX_SHIFT 16

#define KVASER_PCIEFD_KCAN_STAT_SEQNO_SHIFT 24
#define KVASER_PCIEFD_KCAN_STAT_SRP BIT(6)
/* Abort request */
#define KVASER_PCIEFD_KCAN_STAT_AR BIT(7)
#define KVASER_PCIEFD_KCAN_STAT_TXFR BIT(8)
#define KVASER_PCIEFD_KCAN_STAT_IRQ BIT(9)
/* Idle state. Controller in reset mode and no abort or flush pending */
#define KVASER_PCIEFD_KCAN_STAT_IDLE BIT(10)
/* Bus off */
#define KVASER_PCIEFD_KCAN_STAT_BOFF BIT(11)
#define KVASER_PCIEFD_KCAN_STAT_TXE BIT(12)
#define KVASER_PCIEFD_KCAN_STAT_TXI BIT(13)
/* Reset mode request */
#define KVASER_PCIEFD_KCAN_STAT_RMR BIT(14)
/* Controller in reset mode */
#define KVASER_PCIEFD_KCAN_STAT_IRM BIT(15)
/* Controller got one-shot capability */
#define KVASER_PCIEFD_KCAN_STAT_CAP BIT(16)
/* Controller got CAN FD capability */
#define KVASER_PCIEFD_KCAN_STAT_FD BIT(19)
#define KVASER_PCIEFD_KCAN_STAT_BUS_OFF_MSK (KVASER_PCIEFD_KCAN_STAT_AR | \
	KVASER_PCIEFD_KCAN_STAT_BOFF | KVASER_PCIEFD_KCAN_STAT_RMR | \
	KVASER_PCIEFD_KCAN_STAT_IRM)

/* Reset mode */
#define KVASER_PCIEFD_KCAN_MODE_RM BIT(8)
/* Listen only mode */
#define KVASER_PCIEFD_KCAN_MODE_LOM BIT(9)
#define KVASER_PCIEFD_KCAN_MODE_DWH BIT(10)
#define KVASER_PCIEFD_KCAN_MODE_MRRQ BIT(11)
/* Error packet enable */
#define KVASER_PCIEFD_KCAN_MODE_EPEN BIT(12)

#define KVASER_PCIEFD_KCAN_MODE_SM BIT(13)
#define KVASER_PCIEFD_KCAN_MODE_ROP BIT(14)
/* CAN FD non-ISO */
#define KVASER_PCIEFD_KCAN_MODE_NIFDEN BIT(15)
/* Acknowledgment packet type */
#define KVASER_PCIEFD_KCAN_MODE_APT BIT(20)
#define KVASER_PCIEFD_KCAN_MODE_AAM BIT(21)
/* Active error flag enable. Clear to force error passive */
#define KVASER_PCIEFD_KCAN_MODE_EEN BIT(23)
/* Disable protocol exception */
#define KVASER_PCIEFD_KCAN_MODE_DPE BIT(30)
/* Classic CAN mode */
#define KVASER_PCIEFD_KCAN_MODE_CCM BIT(31)

#define KVASER_PCIEFD_KCAN_BTRN_SJW_SHIFT 13
#define KVASER_PCIEFD_KCAN_BTRN_TSEG1_SHIFT 17
#define KVASER_PCIEFD_KCAN_BTRN_TSEG2_SHIFT 26

#define KVASER_PCIEFD_KCAN_PWM_TOP_SHIFT 16

/* Kvaser KCAN packet types */
#define KVASER_PCIEFD_PACK_TYPE_DATA 0
#define KVASER_PCIEFD_PACK_TYPE_ACK 1
#define KVASER_PCIEFD_PACK_TYPE_TXRQ 2
#define KVASER_PCIEFD_PACK_TYPE_ERROR 3
#define KVASER_PCIEFD_PACK_TYPE_EFLUSH_ACK 4
#define KVASER_PCIEFD_PACK_TYPE_EFRAME_ACK 5
#define KVASER_PCIEFD_PACK_TYPE_ACK_DATA 6
#define KVASER_PCIEFD_PACK_TYPE_STATUS 8
#define KVASER_PCIEFD_PACK_TYPE_BUS_LOAD 9

/* Kvaser KCAN packet common definitions */
#define KVASER_PCIEFD_PACKET_SEQ_MSK 0xff
#define KVASER_PCIEFD_PACKET_CHID_SHIFT 25
#define KVASER_PCIEFD_PACKET_TYPE_SHIFT 28

/* Kvaser KCAN TDATA and RDATA first word */
#define KVASER_PCIEFD_RPACKET_IDE BIT(30)	/* identifier extension */
#define KVASER_PCIEFD_RPACKET_RTR BIT(29)	/* remote transmission request */
/* Kvaser KCAN TDATA and RDATA second word */
#define KVASER_PCIEFD_RPACKET_ESI BIT(13)	/* error state indicator */
#define KVASER_PCIEFD_RPACKET_BRS BIT(14)	/* bit rate switch */
#define KVASER_PCIEFD_RPACKET_FDF BIT(15)	/* flexible data rate format */
#define KVASER_PCIEFD_RPACKET_DLC_SHIFT 8
/* Kvaser KCAN TDATA second word */
#define KVASER_PCIEFD_TPACKET_SMS BIT(16)
#define KVASER_PCIEFD_TPACKET_AREQ BIT(31)

/* Kvaser KCAN APACKET */
#define KVASER_PCIEFD_APACKET_FLU BIT(8)	/* Flushed Packet.
 If this bit is set it indicates that the packet was not handled by the
 hardware because of a flush or abort operation. */
#define KVASER_PCIEFD_APACKET_CT BIT(9)		/* ACK packet is generated by a control packet being finished.*/
#define KVASER_PCIEFD_APACKET_ABL BIT(10)	/* arbitration lost */
#define KVASER_PCIEFD_APACKET_NACK BIT(11)	/* a single shot packet was not transmitted */

/* Kvaser KCAN SPACK first word */
#define KVASER_PCIEFD_SPACK_RXERR_SHIFT 8
#define KVASER_PCIEFD_SPACK_BOFF BIT(16)
#define KVASER_PCIEFD_SPACK_IDET BIT(20)
#define KVASER_PCIEFD_SPACK_IRM BIT(21)
#define KVASER_PCIEFD_SPACK_RMCD BIT(22)
/* Kvaser KCAN SPACK second word */
#define KVASER_PCIEFD_SPACK_AUTO BIT(21)
#define KVASER_PCIEFD_SPACK_EWLR BIT(23)
#define KVASER_PCIEFD_SPACK_EPLR BIT(24)




#define KVASER_PCIEFD_WAIT_TIMEOUT msecs_to_jiffies(1000)
#define KVASER_PCIEFD_BEC_POLL_FREQ (jiffies + msecs_to_jiffies(200))
#define KVASER_PCIEFD_MAX_ERR_REP 256
#define KVASER_PCIEFD_CAN_TX_MAX_COUNT 17
#define KVASER_PCIEFD_MAX_CAN_CHANNELS 4
#define KVASER_PCIEFD_DMA_COUNT 2

#define KVASER_PCIEFD_DMA_SIZE (4 * 1024)
#define KVASER_PCIEFD_64BIT_DMA_BIT BIT(0)

/* Kvaser KCAN CAN controller registers */
#define KVASER_PCIEFD_KCAN0_BASE 0x10000
#define KVASER_PCIEFD_KCAN_BASE_OFFSET 0x1000
#define KVASER_PCIEFD_KCAN_FIFO_REG 0x100
#define KVASER_PCIEFD_KCAN_FIFO_LAST_REG 0x180
#define KVASER_PCIEFD_KCAN_CTRL_REG 0x2c0
#define KVASER_PCIEFD_KCAN_CMD_REG 0x400	/* 0x100 * 4 */
#define KVASER_PCIEFD_KCAN_IOC_REG 0x404	/* 0x101 * 4 IO control */
#define KVASER_PCIEFD_KCAN_IEN_REG 0x408	/* 0x102 * 4 */
#define KVASER_PCIEFD_KCAN_ISTAT_REG 0x40c	/* 0x103 * 4  Interrupt status */
#define KVASER_PCIEFD_KCAN_IRQ_REG 0x410	/* 0x104 * 4  IRQ int reqeust */
#define KVASER_PCIEFD_KCAN_TX_NPACKETS_REG 0x414
#define KVASER_PCIEFD_KCAN_STAT_REG 0x418
#define KVASER_PCIEFD_KCAN_MODE_REG 0x41c	/* 0x107 * 4 */
/* nominal bit timing */ 
#define KVASER_PCIEFD_KCAN_BTRN_REG 0x420	/* 0x108 * 4 */
#define KVASER_PCIEFD_KCAN_BLP_REG 0x424	/* 0x109 * 4  Bus Load Prescaler */
/* date phase bit timing */
#define KVASER_PCIEFD_KCAN_BTRD_REG 0x428
#define KVASER_PCIEFD_KCAN_PWM_REG 0x430	/* 0x10c  Transmitter Power Control */

#define KVASER_PCIEFD_KCAN_HWCAP_REG 0x438	/* 0x10e Hardware Capabilities */


/* Hardware Capability bits, KCAN_HWCAP_REG */

#define KVASER_KCAN_HWCAP_TRL	BIT(31)
#define KVASER_KCAN_HWCAP_PWM	BIT(30)
#define KVASER_KCAN_HWCAP_BL	BIT(29)


/* even these structures are from the Kvaser SocketCAN */
struct kvaser_pciefd;

struct kvaser_pciefd_can {
        u8 minor;		/* the drivers minor used for this CAN */
	struct can_priv can;
	struct kvaser_pciefd *kv_pcie;
	void __iomem *reg_base;
	struct can_berr_counter bec;
	u8 cmd_seq;
	int err_rep_cnt;
	int echo_idx;
	spinlock_t lock; /* Locks sensitive registers (e.g. MODE) */
	spinlock_t echo_lock; /* Locks the message echo buffer */
	struct timer_list bec_poll_timer;
	struct completion start_comp, flush_comp;
};

struct kvaser_pciefd {
	struct pci_dev *pci;
	void __iomem *reg_base;			/* pci register base address */
	struct kvaser_pciefd_can *can[KVASER_PCIEFD_MAX_CAN_CHANNELS];
	void *dma_data[KVASER_PCIEFD_DMA_COUNT];
	u8 nr_channels;
	u32 bus_freq;
	u32 freq;
	u32 freq_to_ticks_div;
};


struct kvaser_pciefd_rx_packet {
	u32 header[2];
	u64 timestamp;
};

struct kvaser_pciefd_tx_packet {
	u32 header[2];
	u8 data[64];
};

static const struct can_bittiming_const kvaser_pciefd_bittiming_const = {
	.name = KVASER_PCIEFD_DRV_NAME,
	.tseg1_min = 1,
	.tseg1_max = 255,
	.tseg2_min = 1,
	.tseg2_max = 32,
	.sjw_max = 16,
	.brp_min = 1,
	.brp_max = 4096,
	.brp_inc = 1,
};


void can_showpackagetype(int type);


