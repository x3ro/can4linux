# can4linux version 4.6

The Linux CAN device driver

- contains now a virtual CAN mode which can be used without real CAN hardware.
   use
```   make TARGET=GENERIC
```
- supports CAN FD with a payload length of up to 64 byte
- support of the Xilinx Zynq CAN controller for the Zedboard  
- The new `TARGET=KVASER_PCICANFD`
  supports the 4 channel Kvaser CAN FD PCIe board

For more information check the can4linux/INSTALL.pdf
and the
[can4linux web site](http://can-wiki.info/can4linux/man/index.html).


Don't use the old tgz archive, download the latest source code at
[GitLab](https://gitlab.com "GitLab") using **git**.

```bash
  git clone https://gitlab.com/hjoertel/can4linux.git
```
***
Anyone like to join the development should at least give some information
on the reason for joining, please. 
***


## Installing and using the virtual can4linux CAN device driver

*can4linux* is supporting many different CAN Interface hardware, external boards
or internal on-chip CAN controller.
Nevertheless, sometimes no hardware is available but one want to learn how CAN
works, or want to test a higher layer CAN protocol like _CANopen_ or _J1939_.
For this reason, can4linux supports what we call a virtual CAN bus Interface.

To get it working, to different approaches are possible:

- use the can4linux driver specially compiled to use no hardware. In this case
  it is compiled calling:

```bash
   		make TARGET=GENERIC
   		sudo make load  
```
   	or  

```bash
	sudo insmod can4linux.ko
```
- use a version with hardware support and load the driver wit a special flag

```bash
		make TARGET=whatever_target
		sudo make simload
```
	or

```bash
		sudo insmod virtual=1 can4linux.ko
```
