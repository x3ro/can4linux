# Make some example applications using can4linux device driver
# 
# This can be used building stand alone applications on a PC Linux
# for Crosscompiling set CC when calling make
# e.g. for MPLs VCMA9 say:
# make CC=/opt/toolchains/bin/arm-linux-gcc-3.3.2
#
# 
# TAKE CARE to compile with -DCANFD if the used can4linux
# is compiled with the CAN FD message structure as well. 
#
# The TARGET selection mostly selects a cross compiler platform
#
# TARGET=ZED		can be used for the Zedboard or the DAVE BORA
# TARGET=LINUX_X86	Standard Linux using the native compiler
# 			not only X86 but as well ARM or others
#
#
TARGET=LINUX_X86

BINDIR=/usr/local/bin	# install dir für executables
HOMEBIN=$(HOME)/bin

# depending on how the can4linux device driver is compiled
# should be CANFD since 2018
USECANFD=
USECANFD=-DCANFD

# ============================================================================
# Target indepentant definitions
CTAGS =	ctags --c-types=dtvf
CTAGS =	elvtags -tsevl
CTAGS =	etags  

# ============================================================================
ifeq "$(TARGET)" "LINUX_X86"
#ARCH  = -m32

#CFLAGS = -Wall -I/usr/src/can4linux -DUSE_RT_SCHEDULING
CFLAGS = -Wall -I. 
CFLAGS = -g -Wall -pedantic -I. -DUSE_RT_SCHEDULING $(USECANFD)
#CC=clang
#CFLAGS = -Weverything -I. -DUSE_RT_SCHEDULING $(USECANFD)
#CFLAGS =  -I. -O2 -DUSE_RT_SCHEDULING $(USECANFD)
endif

# ============================================================================
# Xilinx Zynq,
ifeq "$(TARGET)" "ZED"
    PATH=/opt/Xilinx/14.3/SDK/SDK/gnu/arm/lin64/bin/:/bin:/usr/bin
CROSS  = arm-xilinx-linux-gnueabi-
CFLAGS = -Wall -I. $(USECANFD)
CFLAGS = -g -Wall -I. $(USECANFD)
endif

# ============================================================================
# Hansenhof Electronic
ifeq "$(TARGET)" "MULTILOG32"
    PATH=/home/hansenhof/toolchain/sysroots/x86_64-hh-linux/usr/bin/armv4t-hh-linux-gnueabi:/bin:/usr/bin
CROSS  = arm-hh-linux-gnueabi-
CFLAGS = -Wall -I. $(USECANFD)\

endif

# ============================================================================
# EMS Wünsche EtherCAN ethernet to CAN Gateway
ifeq "$(TARGET)" "ECAN9"
    PATH=/opt/OSELAS.Toolchain-2011.11.3/arm-v5te-linux-gnueabi/gcc-4.6.2-glibc-2.14.1-binutils-2.21.1a-kernel-2.6.39-sanitized/bin:/bin:/usr/bin
CROSS  = arm-v5te-linux-gnueabi-
CFLAGS = -Wall -I. $(USECANFD)\

endif

# ============================================================================
ifeq "$(TARGET)" "BEAGLEBONE"
    PATH=/home/oe/development/bb/setup-scripts/build/tmp-angstrom_v2012_12-eglibc/sysroots/x86_64-linux/usr/bin/armv7a-vfp-neon-angstrom-linux-gnueabi:/bin:/usr/bin

CROSS  = arm-angstrom-linux-gnueabi-
CFLAGS = -Wall -I. $(USECANFD)\

endif

# ============================================================================
#  SSV IGW900 Gateway
ifeq "$(TARGET)" "LINUX_COLDFIRE"

CROSS = m68k-elf-
ARCH  = -m5307

# IGW900
UCLIBBASE = /home/oertel/pakete/uClinux-dist-20030909-SSV20040610/lib
LIBS  = \
	-L$(UCLIBBASE)/uClibc/. \
	-L$(UCLIBBASE)/uClibc/lib \
	-L$(UCLIBBASE)/libm \
	-L$(UCLIBBASE)/libnet \
	-L$(UCLIBBASE)/libdes \
	-L$(UCLIBBASE)/libaes \
	-L$(UCLIBBASE)/libpcap \
	-L$(UCLIBBASE)/libssl \
	-L$(UCLIBBASE)/libcrypt_old \
	-L$(UCLIBBASE)/libsnapgear++ \
	-L$(UCLIBBASE)/libsnapgear \
	-L$(UCLIBBASE)/zlib \

LDFLAGS = $(CFLAGS) -Wl,-elf2flt -Wl,-move-rodata -nostartfiles  \
	$(UCLIBBASE)/uClibc/lib/crt0.o -Wa,-m5307
DEFS    = -DCONFIG_COLDFIRE -Dlinux -D__linux__ -Dunix -D__uClinux__ -DEMBED -DCOLDFIRE 
          
CFLAGSX = -Os -fomit-frame-pointer -fno-builtin -msep-data 

endif

# ============================================================================
ifeq "$(TARGET)" "LINUX_POWERPC"
CROSS=powerpc-linux-
ARCH=missing

CFLAGS = -Wall -I. -DUSE_RT_SCHEDULING 
endif

# ============================================================================
ifeq "$(TARGET)" "LINUX_BLACKFIN"
PATH += :/opt/uClinux/bfin-uclinux/bin
CROSS=/opt/uClinux/bfin-uclinux/bin/bfin-uclinux-
ARCH=

AS=
AR=
LD=

CFLAGS  =  $(ARCH) $(CFLAGSX) -I. -D$(TARGET) $(DEFS)
ASFLAGS	=
INCDIR	=

LDFLAGS = $(CFLAGS) -Wl,-elf2flt 
	
DEFS    = -DCONFIG_BFIN -Dlinux -D__linux__ -Dunix -D__uClinux__ \
	-DEMBED -DBFIN  -Wall
DEFS    = -Wall
CFLAGSX = -Os -fomit-frame-pointer -fno-builtin -msep-data
CFLAGSX =

endif
# ============================================================================

CC= $(CROSS)gcc


all: ctest baud can_send acceptance noiser receive transmit \
	can_verify receive-select transmit-select \
	transmitblock dump-struct rcnt canthread

git_track.h:	version
.PHONY: version
version:
	./git_track


.PHONY: test
test:
	@echo "PATH $(PATH)"
	@echo "CROSS $(CROSS)"
	@echo "CC $(CC)"
	@$(CC)


# simple applications, using the simple build-in make rule
ctest:			ctest.o	can4linux.h
can_verify:		can_verify.o	can4linux.h
can_replay:		can_replay.o libcan.o	can4linux.h
baud:			baud.o	can4linux.h
acceptance:		acceptance.o	can4linux.h
noiser:			noiser.o	can4linux.h
receive:		receive.o	can4linux.h git_track.h
listen-select:		listen-select.o	can4linux.h
receive-select:		receive-select.o	can4linux.h
transmit-select:	transmit-select.o	can4linux.h
transmit:		transmit.o	can4linux.h
transmitblock:		transmitblock.o
transmit2:		transmit2.o
transmit3:		transmit3.o
dump-struct:		dump-struct.o	can4linux.h
rcnt:			rcnt.o	can4linux.h
canled:			canled.c	can4linux.h
canterm:		canterm.c	can4linux.h
timertest:		timertest.c	can4linux.h

can_send:		can_send.o
	$(CC) $(CFLAGS) $(LDFLAGS)  can_send.o -lc -o $@

canthread:		canthread.o
	$(CC) $(CFLAGS) $(LDFLAGS)  canthread.o -lc  -pthread -o $@

ctags:
	$(CTAGS) can_send.c can4linux.h



# create a shared object file for TCL
# (tested with SWIG1.1-883 and 2.0.4,  must be installed)
# Test with canLtwo.tcl
canLtwo.so:	canLtwo.c canLtwo.i ../can4linux/can4linux.h
	swig -tcl canLtwo.i
	$(CC) $(CFLAGS) -c -fpic -I/usr/local/include -I/usr/include/tcl8.5 \
	    canLtwo.c canLtwo_wrap.c
	$(CC) -shared canLtwo.o canLtwo_wrap.o -o $@

pyCan:	pyCan.c pyCan.i ../can4linux/can4linux.h
	swig -python pyCan.i
	$(CC) $(CFLAGS) -c -fpic $$(python-config --includes) \
	    pyCan.c pyCan_wrap.c
	$(CC) -shared pyCan.o pyCan_wrap.o -o _pyCan.so
	sed -e 's/can_open = _pyCan.can_open/open  = _pyCan.can_open /' -i pyCan.py
	sed -e 's/can_close = _pyCan.can_close/close = _pyCan.can_close /' -i pyCan.py
	sed -e 's/can_send = _pyCan.can_send/send  = _pyCan.can_send /' -i pyCan.py
	sed -e 's/can_read = _pyCan.can_read/read  = _pyCan.can_read /' -i pyCan.py
	sed -e 's/can_read1 = _pyCan.can_read1/read1 = _pyCan.can_read1 /' -i pyCan.py
	sed -e 's/can_read2 = _pyCan.can_read2/read2 = _pyCan.can_read2 /' -i pyCan.py

clean:
	-rm -f *.o *.gdb gmon.out \
		ctest acceptance baud can_verify \
		canled \
		canterm \
		transmit transmit2 transmit3 \
		transmit-select transmitblock \
		dump-struct \
		can_replay can_send \
		receive \
		receive-select listen-select \
		noiser \
		rcnt \
		canLtwo.so

# run Flexelint on can_send.c
lint:
	flint  gcc-include-path.lnt size-options.lnt co-gnu3.lnt \
		-format='%f%(:%l%)%(:%c:%)%t\s%n:%m' \
		can_send.c
.PHONY: installlocal
installlocal:
	sudo cp can_send receive noiser receive-select \
		ctest canled baud acceptance \
		$(HOMEBIN)

.PHONY: install
install:
	sudo cp can_send receive noiser receive-select \
		ctest canled baud acceptance \
		$(BINDIR)

help:
	@echo "make installlocal - installs at home/bin ~"
	@echo "make install - installs at $(BINDIR)"
	@echo "compiles with CFD structures"
	@echo " otherwise specify USECANFD=  (e.g. for RaspberryPi od BananaPi)"
